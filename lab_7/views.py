from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()
# csui_helper = CSUIhelper(os.environ.get("SSO_USERNAME", "yourusername"),
#                          os.environ.get("SSO_PASSWORD", "yourpassword")

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list, next_page, prev_page = csui_helper.instance.get_mahasiswa_list()

    friend_list = Friend.objects.all()
    response = {
        "mahasiswa_list": mahasiswa_list, 
        "friend_list": friend_list,
        "next_page_num": next_page,
        "prev_page_num": prev_page,
    }
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar-teman.html'
    return render(request, html, response)

def friend_list_json(request,friend_id): # update
    friends = [obj.as_dict() for obj in Friend.objects.filter(npm=friend_id)]
    return JsonResponse({"results": friends}, content_type='application/json')


@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']

        try:
            db_data = Friend.objects.get(npm=npm)
            return HttpResponseBadRequest()
        except Friend.DoesNotExist:
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data = model_to_dict(friend)
            return HttpResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(npm=friend_id).delete()
    return HttpResponseRedirect('/lab-7/get-friend-list')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    try:
        db_friend = Friend.objects.get(npm=npm)
        is_taken = True
    except Friend.DoesNotExist:
        is_taken = False
     
    data = {
        'is_taken': is_taken,#lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

def get_page_data_by_page_number(request, page_number):
    mahasiswa_list, next_page, prev_page = csui_helper.instance.get_mahasiswa_list_by_page(page_number)
    data = {
        "mahasiswa_list": mahasiswa_list,
        "next_page_num": next_page,
        "prev_page_num": prev_page,
    }
    return JsonResponse(data)

def get_friend_data(request):
    data = serializers.serialize('json', Friend.objects.all())
    struct = json.loads(data)

    data = []
    for i in range(len(struct)):
        data.append(eval(json.dumps(struct[i]["fields"])))
    return JsonResponse({"data":data})