// Calculator
var print = document.getElementById('print');
var erase = false;
var counter = 0;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END
function minimize() { 
  var element = document.getElementById("arrow"); 
  if (element.className === "normal") { 
    $(".chat-body").hide(); 
    element.className = "rotate"; 
  } 
  else if ( element.className === "rotate") { 
    $(".chat-body").show(); 
    element.className = 'normal'; 
  } 
}

function insertChat(chat) {
  var addedChat;
  if (counter % 2 == 0) {
    addedChat = '<div class="msg-send">' + chat + '</div>';
  } else {
    addedChat = '<div class="msg-receive">' + chat + '</div>';
  }
  counter++;
  $('.msg-insert').append(addedChat);
}

$(function () {
    $("#enter").keypress(function (e) {
      var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
          insertChat(e.target.value);
          $('#enter').val('');
        }
    });
});